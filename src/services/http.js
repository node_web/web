import axios from 'axios'
var http = null
if (process.env.NODE_ENV == 'production') {
    http = axios.create({})
} else {
    http = axios.create({
        baseURL: 'http://localhost:3000'
    })
}
export default http
