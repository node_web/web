import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path:'',
        name: 'Dashboard',
        component: () => import(/* webpackChunkName: "about" */ '../views/Dashboard.vue')
      },
      {
        path:'new',
        name: 'New User',
        component: () => import(/* webpackChunkName: "about" */ '../views/Users/New.vue')
      },
      {
        path:'users/:_id',
        name: 'Edit User',
        component: () => import(/* webpackChunkName: "about" */ '../views/Users/Edit.vue')
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    
  }
]

const router = new VueRouter({
  routes
})

export default router
